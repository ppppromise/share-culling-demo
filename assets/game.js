// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        tileMap: cc.TiledMap,
        tileAssest: cc.TiledMapAsset,
    },
    onLoad () {
        this.tileMap.node.on(cc.Node.EventType.TOUCH_MOVE, this.onMapMove, this);
    },

    start () {
        this.tileMap.tmxAsset = this.tileAssest;
    },

    /**
     * 重写tiledmap的相关函数，实现share culling
     */
    overrideTiledMapFunction () {
        let func = cc.TiledLayer.prototype._updateCulling;
        cc.TiledLayer.prototype._updateCulling = function () {
            console.time();
            func();
            console.timeEnd();
        };
    
    },
    onMapMove (event) {
        let {x ,y} = event.getDelta();

        this.tileMap.node.x += x;
        this.tileMap.node.y += y;
    },


    update (dt) {
        console.log("===")
    },
});
